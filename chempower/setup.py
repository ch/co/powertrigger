from setuptools import setup
from version import chempower_version
import multiprocessing
import sys
import os

script_list = [ os.path.join('bin',x) for x in os.listdir('bin')]

setup(
    name='ChemPower',
    version=chempower_version,
    author='Catherine Pitt',
    author_email='cen1001@cam.ac.uk',
    packages=['chempower'],
    scripts=script_list,
    description='Chemistry power management module',
    install_requires=['argparse','future'],
    license='GPL'
)
