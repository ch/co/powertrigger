from builtins import object
import subprocess

class PDUControl(object):
    '''Abstract class defining available power control methods'''

    def off(outlet):
        raise NotImplemented

    def on(outlet):
        raise NotImplemented

    def status(outlet):
        raise NotImplemented

    def _run_cmd(self,cmd):
        try:
            output = subprocess.Popen(cmd,stderr=subprocess.PIPE,
                                          stdout=subprocess.PIPE).communicate()[0].decode('utf-8')
        except subprocess.CalledProcessError as e:
            output = 'Command {0} returned {1} with message "{2}"'.format(' '.join(cmd),
                                                                     e.returncode,
                                                                     e.output)
        return output


class SSHNetSNMP(PDUControl):
    """
    Power control class for APC PDUs attached to a remote host accessed by ssh.
    
    Use this class to control hosts fed by PDUs that are part of a cluster and can
    be accessed via SNMP. The default oid works for firmware 3.7.4 of APC AP7953 but firmware 3.3.4 uses 
    enterprises.apc.products.hardware.rPDU.rPDUOutlet.rPDUOutletControl.rPDUOutletControlTable.rPDUOutletControlEntry.rPDUOutletControlOutletCommand.
    The cluster needs to have netsnmp installed and suitable config files and mibs available.  
    """

    def __init__(self,pdu,socket,username=None,hostname='localhost',community=None,oid=None):
        self.pdu = pdu
        self.socket = socket
        self.hostname = hostname
        self.ssh_options = ['-o','BatchMode=yes','-o','StrictHostKeyChecking=no']
        if username:
            self.ssh_options.extend(['-l',username])
        self.snmp_options = ['-v','1',pdu]
        if community:
            self.snmp_options.extend(['-c',community])
        if not oid:
            self.oid = 'enterprises.apc.products.hardware.masterswitch.sPDUOutletControl.sPDUOutletControlTable.sPDUOutletControlEntry.sPDUOutletCtl.' + socket
        else:
            self.oid = oid + socket
        self.ssh_cmd = ['ssh'] + self.ssh_options + [hostname]

    def on(self):
        cmd = self._build_cmd('snmpset','i','1')
        output = self._run_cmd(cmd)
        return output

    def off(self):
        cmd = self._build_cmd('snmpset','i','2')
        output = self._run_cmd(cmd)
        return output

    def status(self):
        cmd = self._build_cmd('snmpget')
        output = self._run_cmd(cmd)
        if 'outletOn' in output or 'immediateOn' in output:
            state = 'on'
        elif 'outletOff' in output or 'immediateOff' in output:
            state = 'off'
        else:
            state = 'unknown, error was ' + output
        return 'Outlet {0} on {1} PDU {2} state is {3}'.format(self.socket,
                                                           self.hostname,
                                                           self.pdu,
                                                           state)

    def _build_cmd(self,snmp_action,Type=None,value=None):
        set_options = []
        if Type and value:
            set_options = [Type, value]
        cmd = self.ssh_cmd + [snmp_action] + self.snmp_options + [self.oid] + set_options
        return cmd


class DirectNetSNMP(SSHNetSNMP):
    """Power management for machines on PDUs connected to the main network"""

    def _build_cmd(self,snmp_action,Type=None,value=None):
        set_options = []
        if Type and value:
            set_options = [Type, value]
        cmd = [snmp_action] + self.snmp_options + [self.oid] + set_options
        return cmd


class DirectIpmi(PDUControl):
    """Power management for machines on IPMI on the same network"""

    def __init__(self,hostname,username='ADMIN',password='ADMIN',
                 ipmitool='ipmitool'):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.ipmitool = ipmitool
        self.ipmi_options = ['-H',hostname,'-U',username,'-P',password]

    def status(self):
        cmd = self._build_cmd(['power','status'])
        output = self._run_cmd(cmd)
        if 'on' in output:
            state = 'on'
        elif 'off' in output:
            state = 'off'
        else:
            state = 'unknown'
        return 'IPMI {0} power state is {1}'.format(self.hostname,state)

    def on(self):
        cmd = self._build_cmd(['power','on'])
        output = self._run_cmd(cmd)
        return output

    def off(self):
        cmd = self._build_cmd(['power','off'])
        output = self._run_cmd(cmd)
        return output

    def _build_cmd(self,ipmi_args):
        cmd = [self.ipmitool] + self.ipmi_options + ipmi_args
        return cmd
