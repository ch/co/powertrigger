from __future__ import absolute_import
from . import control

old_oid = 'enterprises.apc.products.hardware.rPDU.rPDUOutlet.rPDUOutletControl.rPDUOutletControlTable.rPDUOutletControlEntry.rPDUOutletControlOutletCommand.'

clients = {'splot4' : [ [control.SSHNetSNMP, {'username' : 'pdumanager',
                                       'hostname' : 'odyssey.ch.cam.ac.uk',
                                       'pdu' : 'pdu1',
                                       'socket' : '17'}, 
                        ],
                         [control.SSHNetSNMP, {'hostname' : 'odyssey.ch.cam.ac.uk',
                                       'username' : 'pdumanager',
                                       'pdu' : 'pdu2',
                                       'socket' : '17'}, 
                         ], 
                      ],
           'mvfs-c' : [ [control.SSHNetSNMP, {'username' : 'pdumanager',
                                       'hostname' : 'odyssey.ch.cam.ac.uk',
                                       'pdu' : 'pdu1',
                                       'socket' : '20'}, 
                        ],
                        [control.SSHNetSNMP, {'username' : 'pdumanager',
                                       'hostname' : 'odyssey.ch.cam.ac.uk',
                                       'pdu' : 'pdu2',
                                       'socket' : '18'}, 
                        ], 
                      ],
           'fake01' : [ [control.SSHNetSNMP, {'username' : 'pdumanager',
                                              'hostname' : 'ziggy.ch.cam.ac.uk',
                                              'pdu' : 'power-0-0',
                                              'socket' : '23'
                                             } 
                        ],
                        [control.SSHNetSNMP, {'username' : 'pdumanager',
                                              'hostname' : 'ziggy.ch.cam.ac.uk',
                                              'pdu' : 'power-0-1',
                                              'socket' : '23'
                                             } 
                        ],
                       ],
           # Direct example
           'g34afake' : [ [ control.DirectNetSNMP, { 'pdu' : 'g34a-apc-1.ipmi.private.ch.cam.ac.uk',
                                                     'socket' : '19'
                                                   }
                          ]
                        ],
           # Direct example with older firmware using different OID
           'mek-quake-compute-0-0' : [ [ control.DirectNetSNMP, { 'pdu' : 'wcdc-rack12-apc2.ipmi.private.ch.cam.ac.uk',
                                                                  'socket' : '15',
                                                                  'oid' : old_oid
                                                                }
                                        ]
                                    ],
           }
