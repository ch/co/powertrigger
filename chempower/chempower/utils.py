"""
Odd useful functions
"""
import re

def cluster_ipmi_name(node,
                      ipmi_prefix='ipmi-0',
                      ipmi_suffix='.ipmi',
                      compute_prefix='comp-0'):
    "Given a cluster nodename, return the IPMI name for the nodei. Defaults for volkhan."
    return re.sub(compute_prefix,ipmi_prefix,node) + ipmi_suffix

