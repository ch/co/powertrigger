from setuptools import setup
from version import powertrigger_version
import multiprocessing
import sys
import os
import glob

template_list = glob.glob('powertrigger/templates/*.html')

setup(
    name='PowerTrigger',
    version=powertrigger_version,
    author='Catherine Pitt',
    author_email='cen1001@cam.ac.uk',
    packages=['powertrigger'],
    description='Trigger app for power management',
    data_files=[('/usr/lib/powertrigger',['powertrigger.wsgi'])],
    package_data={'powertrigger': ['templates/*.html']},
    install_requires=['Flask','future'],
    license='GPL'
)
