from flask import Flask, request, render_template, url_for
import chempower.config as config
import chempower.control as control

app = Flask('powertrigger')

@app.route('/')
def index():
    urls = [ (x,url_for('select_action_for_host',host=x)) for x in sorted(get_computers()) ]
    message = 'Pick a computer from {}'  
    message = render_template('select_host.html',urls=urls)
    return message

@app.route('/hosts/<host>/<action>')
def perform_action_for_host(host,action):
    valid_actions = get_public_methods()
    if action not in valid_actions:
        return render_template('action_error.html',action=action,valid_actions=valid_actions)
    try:
        sockets=config.clients[host]
    except KeyError:
        return render_template('error.html',host=host)
    results=[]
    for socket in sockets:
        socketclass=socket[0](**socket[1])
        results.append(getattr(socketclass,action)())
    message = render_template('results.html',
                              results=results,
                              host=host,
                              actions=get_action_urls(host))
    return message

@app.route('/hosts/<host>')
def select_action_for_host(host):
    if host in get_computers():
        message = render_template('select_action.html',
                                   host=host,
                                   actions=get_action_urls(host))
    else:
        message = render_template('error.html',host=host)
    return message

@app.route('/sockets/<host>/<socket>/<action>')
def perform_action_for_socket(host,socket,action):
    valid_actions = get_public_methods()
    if action not in valid_actions:
        return render_template('action_error.html',action=action,valid_actions=valid_actions)
    try:
        socketnumber = int(socket)-1
        socket = config.clients[host][socketnumber]
    except (IndexError,ValueError,KeyError):
        return render_template('socket_error.html',host=host,socket=socket)
    instantiated_socket = socket[0](**socket[1])
    result = getattr(instantiated_socket,action)()
    message = render_template('results.html',
                              results=[result],
                              host=host,
                              actions=get_action_urls(host))
    return message

def get_public_methods():
    return [x for x in dir(control.PDUControl) if not x.startswith('_')]

def get_computers():
    return list(config.clients.keys())

def get_action_urls(host):
    urls = [('All sockets {}'.format(x),url_for('perform_action_for_host',host=host,action=x)) for x in get_public_methods()]
    socket_urls = [('Socket {} {}'.format(socket_index,action),
                    url_for('perform_action_for_socket',host=host,socket=socket_index,action=action)) 
                    for socket_index,socket in enumerate(config.clients[host])
                    for action in get_public_methods()]
    return urls + socket_urls

if __name__ == '__main__':
    app.run(debug=True)
